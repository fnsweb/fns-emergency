<div id="emergency">
	<a href="#" class="close"></a>
	<h1>Notice</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, necessitatibus, nobis, saepe delectus illum accusantium velit dolore ab assumenda odio laudantium aperiam unde expedita modi beatae ipsa eos est adipisci.</p>
</div>
<script>
	// Not everyone has jquery you know.
	(function () {
		var the_modal = document.getElementById("emergency");
		the_modal.onclick = function () {
			the_modal.style.display = 'none';
		};
	})();
</script>