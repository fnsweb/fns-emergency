<?php include("inc/header.php"); ?>


<?php include("inc/content2.php"); ?>

<script>

	$(function () {

		var modal_maker = function (header, msg) {
			var out = $("<div id='emergency' ></div>").append("<h1>" +  header +"</h1>").append("<p>" + msg +"</p>").append("<a class='close'/>").hide();
			return out;
		}

		$.ajax('./sentinel/').done( function (data) {
			var header, msg;
			var the_data = data;
			header = data["header"] || "Notice";
			msg = data["message"] || "Stuff happened";
			
			$("body").append(modal_maker(header, msg));
			$('a.close').bind('click', function () { $("#emergency").hide(); })
		});
		

		$('a.open').bind('click', function () { $("#emergency").show(); })
	});

</script>

<?php include("inc/footer.php"); ?>