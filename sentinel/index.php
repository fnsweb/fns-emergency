<?php
// This would be the script that would display the message to everyone.

	$the_message = array("message" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, ducimus, facere optio quasi laudantium in libero! Tempora libero at possimus exercitationem aperiam ");

// We're going to use JSON, because everyone knows JSON.
	$out = json_encode($the_message);

// We tell the client it's getting JSON
	header('Content-type: application/json');
	echo $out;
